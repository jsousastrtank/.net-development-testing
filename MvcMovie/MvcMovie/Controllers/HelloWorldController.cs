﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Text.Encodings.Web;


// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MvcMovie.Controllers
{
    public class HelloWorldController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Welcome(string name, int numtimes, int ID)
        {
            ViewData["Message"] = "Hello" + name;
            ViewData["Numtimes"] = numtimes;

            return View();
           //return HtmlEncoder.Default.Encode($"This is the welcome {name} Action MEhtod number {numtimes} using this ID : {ID}");
        }

        
    }
}
